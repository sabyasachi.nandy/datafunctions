import logging
import numbers
import pandas as pd
import numpy as np
from data_functions.config import SUPPORTED_LIST_TYPES, SUPPORTED_SINGLE_TYPES
from data_functions.data_functions import DataFunctions
from data_functions.exceptions import *

logger = logging.getLogger("main")


def convert_pandas_series_to_numpy_array(np_series, data_type=None):
    if data_type == "float":  # Will be casted to the float always. Give control later
        to_return = np_series.values
        to_return = to_return.astype(float)
    else:
        to_return = np_series.values
    return to_return


def convert_numpy_ndarray_to_numpy_array(np_series, data_type=None):
    return np_series


def convert_list_to_numpy_array(_list, data_type=None):
    if data_type == "float":  # Will be casted to the float always. Give control later
        to_return = np.array(_list)
        to_return = to_return.astype(float)
    else:
        to_return = np.array(_list)
    return to_return


def get_numeric_arguments_index(args_list):
    return [index for (index, x) in enumerate(args_list) if isinstance(x, numbers.Number)]


def check_list(args, obj):
    """
    Check if passed args is of list(Array) type. Later add check for dtype
    :param args: Arg to check
    :param obj: A dict, with obj type, and dtype defined
    :return: True or false, if true send dtype of the args
    """
    logger.info(f"List Check Type {obj['type']}, {obj['dtype']}")
    for _type in SUPPORTED_LIST_TYPES:
        if isinstance(args, _type):
            logger.info(f"Args is of instance {_type}")
            return True, _type
    return False, None


def check_single(args, obj):
    """
    Check if passed args is of single type. Later add check for dtype
    :param args: Arg to check
    :param obj: A dict, with obj type, and dtype defined
    :return: True or false
    """
    logger.info(f"Single Check Type {obj['type']}, {obj['dtype']}")
    for _type in SUPPORTED_SINGLE_TYPES:
        if isinstance(args, _type):
            logger.info(f"Args is of instance {_type}")
            return True, _type
    return False, None


def type_combo_checks(args_list, type_combos, infinite=False):
    """
    Check and match args list withe type combos passed.
    Add true to a list, if type combo is valid, else false
    :param args_list: List of args
    :param type_combos: List of types combo
    :return: A list containing bool value, indicating which combos where valid.
    """

    for combo in type_combos:
        # For a combo to be valid, it must satisfy all the arguments.
        arg_check_list = [False] * len(args_list)
        combo_flag = [False] * len(combo)
        for com_index, obj in enumerate(combo):
            logger.info(f"Combo {obj}")
            obj.update({'args': []})
            obj.update({'predicted_data_type': None})
            for index, args in enumerate(args_list):
                if obj['type'] == "list":
                    status, _type = check_list(args, obj)
                    if status:
                        combo_flag[com_index] = True
                        arg_check_list[index] = True
                        obj['args'].append(args)
                        obj['predicted_data_type'] = _type

                elif obj['type'] == "single":
                    status, _type = check_single(args, obj)
                    if status:
                        combo_flag[com_index] = True
                        arg_check_list[index] = True
                        obj['args'].append(args)
                        obj['predicted_data_type'] = _type
                logger.info(f"{combo_flag}, {arg_check_list}")
        logger.info(f"{combo_flag}, {arg_check_list}")
        if not infinite and all(combo_flag):
            return combo
        elif all(combo_flag) and all(arg_check_list):
            return combo
    return None


def args_validation(args_list, type_combos, arg_limit, *args, **kwargs):
    """
    Get a list of args.
    Check for validation of args based on type combos, and arg limit.
    eg:
        arg_limit - 0 - Specifies infinity.
        arg_limit - 1 - Specifies only one arg can be passed to the function
        type_combos - A list of tuples
            e.g [[{'type': 'list', 'dtype': 'int'}, {'type': 'single', 'dtype': 'int'}]]
             with arg_limit: 2,  Specifies that we are expecting two args,
             out of which one should be list and one int.
            * Note type_combos should have only one one entry per set if args limit is infinite.
    :param args_list: List of args
    :param type_combos: Valid type combination to check for
    :param arg_limit: Limit to number of args
    :param A valid args tuple, with type info
            e.g: (
            {
                'type': 'single',
                'dtype': 'int',
                'args': 1
            },
            {
                'type': 'single',
                'dtype': 'number',
                'args': 1
            }
        )
    :raises Error if args list not valid, or if cant be converted to numpy type.
    """
    if arg_limit:
        logger.info(f"Arg limit is set to {arg_limit}")
        if len(args_list) > arg_limit:
            raise ArgsValidationException(
                message="Number of arguments exceeds limit",
                info={
                    'type': 'internal',
                    'message': "Please check the number of args passed to the function."
                }
            )
    else:
        logger.info(f"Arg limit is set to infinite")
        if any([True for x in type_combos if len(x) > 1]):
            raise ArgsValidationException(
                message="Number of types inn types combo cannot be more than one if args limit is zero",
                info={
                    'type': 'internal',
                    'message': "Please set number of types to maximum one."
                }
            )
    infinite = not bool(arg_limit)
    combo_passed = type_combo_checks(args_list, type_combos, infinite)
    if combo_passed is None:
        raise ArgsValidationException(
            message="The args provided does not match the requirements for this functions",
            info={
                'type': 'client',
                'message': "Please recheck the type of args provided."
            }
        )
    return True, combo_passed


def process_args(args_with_info):
    """
    Convert the args based on the type info supplied with the args.
    :param args_with_info: List of args with info about type and datatype
    :return: Args in valid format.
        e.g:
            [{
                'args': 1,
                'type': 'single',
                'dtype': 'int',
            },
            {
                'args': 10,
                'type': 'single',
                'dtype': 'int',
            }]
    """
    CONVERSION_FUNCTION_LIST = {
        np.ndarray: convert_numpy_ndarray_to_numpy_array,
        pd.Series: convert_pandas_series_to_numpy_array,
        list: convert_list_to_numpy_array
    }

    result = []
    for _args_list in args_with_info:
        if _args_list['type'] == 'list':
            for _arg in _args_list['args']:
                result.append(CONVERSION_FUNCTION_LIST[_args_list['predicted_data_type']](_arg, _args_list['dtype']))
        if _args_list['type'] == 'single':
            for _arg in _args_list['args']:
                if _args_list['type'] == 'int':
                    result.append(int(_arg))
                else:
                    result.append(_arg)
    return result
