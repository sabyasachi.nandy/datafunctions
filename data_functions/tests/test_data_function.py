import os

import pandas as pd
import numpy as np
import pytest
from data_functions.exceptions import DataFunctionsError, DataFunctionsExceptions
from data_functions.functions.basic_arithmetic import SUM
from data_functions.helpers import setup_logger

from data_functions.data_functions import ExplorazorDataFunctions

DF = pd.DataFrame({
    'a': [1, 2, 3, 4],
    'b': [10, 20, 30, 40]
})
PHRAZOR_TEST_DATA = {
    'test.csv': {
        'test': DF
    }
}
setup_logger('main', os.path.join(os.getcwd(), "logs", "main_log.txt"))


def test_composition_explorazor():
    data = ExplorazorDataFunctions(DF)
    assert {'a', 'b'} == set(list(data))


def test_validity_check_explorazor():
    data = ExplorazorDataFunctions(DF)
    assert data.is_valid(SUM(data['a'], data['b']))


def test_false_validity_check_explorazor():
    data = ExplorazorDataFunctions(DF)
    assert not data.is_valid(SUM(data['a'], data['b'], [1, 2]))


def test_execute_fail_explorazor():
    data = ExplorazorDataFunctions(DF)
    with pytest.raises(DataFunctionsExceptions) as e:
        data.execute(SUM(data['a'], data['b'], [1, 2]))
    print("Reason", e.value)


def test_execute_fail_explorazor_1():
    data = ExplorazorDataFunctions(DF)
    with pytest.raises(DataFunctionsExceptions) as e:
        data.execute(SUM(data['a'], 1))
    print("Reason", e.value)


def test_execute_pass_explorazor():
    data = ExplorazorDataFunctions(DF)
    assert isinstance(data.execute(SUM(data['a'], data['b'], [1, 2, 3, 4])), np.ndarray)
    print(data.last_success_value)


def test_execute_pass_recursive_explorazor():
    data = ExplorazorDataFunctions(DF)
    assert isinstance(data.execute(SUM(SUM(data['a'], data['b']), [123, 123, 123, 123])), np.ndarray)
    print(data.last_success_value)


def test_execute_fail_recursive_explorazor():
    data = ExplorazorDataFunctions(DF)
    with pytest.raises(DataFunctionsExceptions) as e:
        data.execute(SUM(SUM(data['a'], data['b'], [1, 2]), [123, 123, 123, 123]))
    print(e.value.info)