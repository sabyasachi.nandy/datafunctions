import os

import pytest

from data_functions.helpers import setup_logger
from data_functions.utils import *

DF = pd.DataFrame({
    'a': [1, 2, 3, 4],
    'b': [10, 20, 30, 40]
})
PHRAZOR_TEST_DATA = {
    'test.csv': {
        'test': DF
    }
}

setup_logger('main', os.path.join(os.getcwd(), "logs", "main_log.txt"))
logger = logging.getLogger('main')


def test_arg_limit_check():
    with pytest.raises(ArgsValidationException):
        args_validation([DF["a"], DF["a"]], arg_limit=1, type_combos=[{
            'type': 'list',
            'dtype': 'int'
        }])


def test_args_validation():
    with pytest.raises(ArgsValidationException) as e:
        args_validation([DF["a"], 1], arg_limit=0, type_combos=[
            [
                {
                    'type': 'list',
                    'dtype': 'int'
                }
            ]
        ])
    print("VALUE", e.value)


def test_valid_type_combo_check():
    args_list = [DF["a"], DF["a"], DF["a"]]
    type_combos = [
        [
            {
                'type': 'list',
                'dtype': 'int'
            },
        ]
    ]
    assert type_combo_checks(args_list, type_combos, infinite=True)

    args_list = [1, 1]
    type_combos = [
        [
            {
                'type': 'single',
                'dtype': 'int'
            },
            {
                'type': 'single',
                'dtype': 'int'
            }
        ]
    ]
    assert type_combo_checks(args_list, type_combos)


def test_in_valid_type_combo_check():
    args_list = [DF["a"], 1]
    type_combos = [
        [
            {
                'type': 'list',
                'dtype': 'int'
            },
        ]
    ]
    assert None == type_combo_checks(args_list, type_combos, infinite=True)

    # args_list = [1, 1]
    # type_combos = [
    #     [
    #         {
    #             'type': 'list',
    #             'dtype': 'int'
    #         },
    #         {
    #             'type': 'single',
    #             'dtype': 'number'
    #         }
    #     ]
    # ]
    # assert None == type_combo_checks(args_list, type_combos)


def test_args_processing():
    args_with_info = (
            {
                'type': 'list',
                'dtype': 'int',
                'args': [DF['a']],
                'predicted_data_type': pd.Series
            },
            {
                'type': 'single',
                'dtype': 'number',
                'args': [1],
                'predicted_data_type': int
            }
    )
    result = [DF['a'].values, 1]
    assert result == process_args(args_with_info)
