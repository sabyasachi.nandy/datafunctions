import os

import pandas as pd
import numpy as np
import pytest
from data_functions.data_functions import ExplorazorDataFunctions
from data_functions.exceptions import DataFunctionInvalidExceptions
from data_functions.functions.basic_arithmetic import SUM, MULTIPLY
from data_functions.helpers import setup_logger

DF = pd.DataFrame({
    'a': [1, 2, 3, 4],
    'b': [10, 20, 30, 40]
})
PHRAZOR_TEST_DATA = {
    'test.csv': {
        'test': DF
    }
}

setup_logger('main', os.path.join(os.getcwd(), "logs", "main_log.txt"))


def test_sum_valid():
    _sum = SUM(DF['a'], DF['b'], [1, 2, 3, 4])
    assert _sum.is_valid()


def test_sum_execute():
    _sum = SUM(DF['a'], DF['b'], [1, 2, 3, 4])
    _list = _sum.execute()
    print("Result", _list)
    assert isinstance(_list, np.ndarray)

    _sum = SUM(DF['a'], DF['b'])
    _list = _sum.execute()
    print("Result", _list)
    assert isinstance(_list, np.ndarray)

    _sum = SUM(DF['a'], DF['b'])
    _list = _sum.execute()
    print("Result", _list)
    assert isinstance(_list, np.ndarray)

    _sum = SUM(np.array(DF['a']), np.array(DF['b']), np.array([1, 2]))
    with pytest.raises(DataFunctionInvalidExceptions):
        _list = _sum.execute()

    _sum = SUM(np.array(DF['a']), np.array(DF['b']), 12)
    with pytest.raises(DataFunctionInvalidExceptions):
        _list = _sum.execute()


def test_multiply_valid():
    _multiply = MULTIPLY(DF['a'], 2)
    assert _multiply.is_valid()


def test_multiply_invalid():
    _multiply = MULTIPLY(DF['a'], DF['a'])
    with pytest.raises(DataFunctionInvalidExceptions):
        _multiply.execute()


def test_multiply_passed():
    _multiply = MULTIPLY(DF['a'], 2)
    result = _multiply.execute()
    print(result)
    assert isinstance(result, np.ndarray)

    _multiply = MULTIPLY(2, DF['a'])
    result = _multiply.execute()
    print(result)
    assert isinstance(result, np.ndarray)
