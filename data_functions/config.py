import numpy as np
import pandas as pd

# This is the supported of type of arguments that data functions can handle
SUPPORTED_LIST_TYPES = [
    np.ndarray,
    pd.Series,
    list,
]

SUPPORTED_SINGLE_TYPES = {
    int,
    float,
    np.int_,
    np.float_
}