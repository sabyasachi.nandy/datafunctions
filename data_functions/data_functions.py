import abc
import logging

import pandas as pd

from data_functions.exceptions import DataFunctionsError, DataFunctionsExceptions, DataFunctionInvalidExceptions
logger = logging.getLogger("main")


class DataFunctions(abc.ABC):

    @abc.abstractmethod
    def name(self):
        return self.name()


class ExplorazorDataFunctions:
    """
    This class will receive the base data obj of explorazor.
    In that we will add extra features over that obj with help of composition.
    We have to type check of the base data obj, and add additional wrapper for each type of base data obj.
    """
    ALLOWED_DATA_TYPES = [pd.DataFrame]
    last_success_value = None

    def __init__(self, data):
        if type(data) not in self.ALLOWED_DATA_TYPES:
            raise NotImplementedError(f"Function for this {type(self.data)} data is not implemented.")
        self.data = data
        self.valid = False
        self.message = False
        self.error = None
        self.base_data_type = type(data)

    def __getattr__(self, attr):
        # Adding the properties of the base data obj to this class
        return getattr(self.data, attr)

    def __getitem__(self, item):
        if isinstance(self.data, pd.DataFrame):
            return self.data[item]
        else:
            raise NotImplementedError(f"Get item for this {type(self.data)} is not implemented.")

    def __iter__(self):
        # Adding __iter__ property, in case anyone calls list(data)
        if isinstance(self.data, pd.DataFrame):  # This is for pandas.
            return iter(set(list(self.data)))
        else:
            return NotImplementedError(f"Iter for type {type(self.data)} not yet implemented.")

    def is_valid(self, _function):
        try:
            obj = _function
            self.valid = obj.is_valid()
            if not self.valid:
                self.error = obj.error
            return self.valid
        except AttributeError as e:
            self.message = f'is_valid function has not implemented in {type(_function)}'
            raise DataFunctionsError(
                message="Invalid function",
                info={
                    'type': 'internal',
                    'message': f'is_valid function has not implemented in {type(_function)}.'
                }
            )
        finally:
            return self.valid

    def execute(self, _function):
        try:
            if not self.valid:
                logger.info("Trying to validate")
                self.is_valid(_function)
            if self.valid:
                logger.info("Function passed is valid")
                self.last_success_value = _function.execute()
                return self.last_success_value
            else:
                logger.error("Function passed is in valid")
                logger.error(f"{self.error}")
                raise DataFunctionsExceptions(message=f"Function is not valid. {self.error['message']}", info=self.error)
        except AttributeError as e:
            raise DataFunctionsError(
                message="Invalid function",
                info={
                    'type': 'internal',
                    'message': f'is_valid function has not implemented in {type(_function)}.'
                }
            )
