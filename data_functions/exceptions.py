class DataFunctionsExceptions(Exception):

    def __init__(self, message, info=None):
        self.message = message
        self.info = info

    def __str__(self):
        return self.message


class DataFunctionsError(Exception):

    def __init__(self, message, info=None):
        self.message = message
        self.info = info

    def __str__(self):
        return self.message


class DataFunctionInvalidExceptions(DataFunctionsExceptions):
    pass


class ArgsValidationException(DataFunctionsExceptions):
    pass
