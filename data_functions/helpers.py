import logging


def setup_logger(logger_name, log_file, level=logging.DEBUG):
    """
    Setup of logger
    :param logger_name: Name of the logger
    :param log_file: The file to write the log to
    :param level: The level to log
    :return:
    """
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(levelname)s %(message)s')
    file_handler = logging.FileHandler(log_file, mode='w')
    file_handler.setFormatter(formatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(file_handler)
    l.addHandler(stream_handler)