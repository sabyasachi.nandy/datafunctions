"""
Contains all the basic arithmetic functions
"""
import logging
import numbers

import numpy as np
import pandas as pd

from data_functions.data_functions import DataFunctions
from data_functions.exceptions import DataFunctionInvalidExceptions, ArgsValidationException
from data_functions.utils import args_validation, process_args

logger = logging.getLogger("main")


class SUM(DataFunctions):
    """
    Adds numpy array, list of same length and returns a numpy array.
    """

    def name(self):
        return "SUM"

    def __init__(self, *args):
        self.args = args
        self.valid = False
        self.valid_args_info = None
        self.error = None

    def __str__(self):
        return type(self)

    def special_validation_checks(self):
        # Additional checks only valid to this class
        _len = len(self.args[0])
        for arg in self.args:
            if len(arg) != _len:
                return False, "Cannot add columns of unequal size"
        return True, "Check Passed"

    def is_valid(self):
        """
        Check if args are valid for this obj.
        :return:
        """
        try:
            value_index = None
            print(f"Type of {type(self.args)}")
            list_of_args = list(self.args)
            for index, args in enumerate(self.args):
                if isinstance(args, DataFunctions):
                    logger.info("Args is a data function")
                    try:
                        logger.info(f"Executing args, {type(args)}")
                        result = args.execute()
                        list_of_args[index] = result
                    except DataFunctionInvalidExceptions as e:
                        raise ArgsValidationException(message=f"Args returning invalid result becuase {e.message}",
                                                      info=e.info)
                    finally:
                        logger.debug(f"Here, error was raised")
            self.args = tuple(list_of_args)
            logger.info("Starting Args Validation")
            self.valid, self.valid_args_info = args_validation(self.args, type_combos=[
                [
                    {
                        'type': 'list',
                        'dtype': 'int'
                    }
                ]
            ], arg_limit=0)
            logger.info("Starting Special Validaiton")
            status, message = self.special_validation_checks()
            logger.info(f"Starting Special Validaiton result {status}")
            if not status:
                raise ArgsValidationException(message=message)
            logger.info(f"Validation result {self.valid}")
            return self.valid
        except ArgsValidationException as e:
            self.valid = False
            self.error = {'message': e.message, 'info': e.info}
        finally:
            return self.valid

    def execute(self):
        """
        Sum data passed in as args and return the value.
        ex -
            sum([1,2,3], [1,2,3])
            returns [2,4,6]

            sum([1,2,3], 1)
            returns [2,3,4]
        :param args: A list of list, or a mixed list numbers and integers
        :returns: A list
        :raises: DataFunctionsExceptions, if sum cannot be performed
        """
        logger.info("Starting Execution of Data Function")
        if not self.valid:
            self.is_valid()
            logger.info(f"Validity Check {self.valid}, {self.error}")
        if self.valid:
            args = process_args(self.valid_args_info)
            logger.info("Processed args")
            array = np.array([*args])
            logger.info("Calculating results")
            return array.sum(axis=0)
        else:
            raise DataFunctionInvalidExceptions(message=self.error['message'], info=self.error['info'])


class MULTIPLY(DataFunctions):
    """
    Multiply numpy array, normal list with a number and returns a numpy array.
    """

    def name(self):
        return "MULTIPLY"

    def __init__(self, *args):
        self.args = args
        self.valid = False
        self.valid_args_info = None
        self.error = None

    def __str__(self):
        return type(self)

    def is_valid(self):
        """
        Check if args are valid for this obj.
        :return:
        """
        try:
            list_of_args = list(self.args)
            for index, args in enumerate(self.args):
                if isinstance(args, DataFunctions):
                    logger.info("Args is a data function")
                    try:
                        logger.info(f"Executing args, {type(args)}")
                        result = args.execute()
                        logger.info(f"Executing done")
                        list_of_args[index] = result
                    except DataFunctionInvalidExceptions as e:
                        logger.info(f"Error executing inner functions")
                        raise ArgsValidationException(message=f"Args returning invalid result becuase {e.message}",
                                                      info=e.info)

            self.args = tuple(list_of_args)
            logger.info("Starting Args Validation")
            self.valid, self.valid_args_info = args_validation(self.args, type_combos=[
                [
                    {
                        'type': 'list',
                        'dtype': 'int'
                    },
                    {
                        'type': 'single',
                        'dtype': 'int'
                    }
                ]
            ], arg_limit=2)
            logger.info(f"Validation result {self.valid}")
            return self.valid
        except ArgsValidationException as e:
            self.valid = False
            self.error = {'message': e.message, 'info': e.info}
        finally:
            return self.valid

    def execute(self):
        """
        Sum data passed in as args and return the value.
        ex -
            sum([1,2,3], [1,2,3])
            returns [2,4,6]

            sum([1,2,3], 1)
            returns [2,3,4]
        :param args: A list of list, or a mixed list numbers and integers
        :returns: A list
        :raises: DataFunctionsExceptions, if sum cannot be performed
        """
        logger.info("Starting Execution of Data Function")
        if not self.valid:
            self.is_valid()
            logger.info(f"Validity Check {self.valid}")
        if self.valid:
            logger.info(f"Validity Check Passed")
            args_1, args_2 = process_args(self.valid_args_info)
            logger.info(f"Process args {type(args_1)} {type(args_2)}")
            if isinstance(args_1, int):
                return args_2*args_1
            else:
                return args_1 * args_2
        else:
            raise DataFunctionInvalidExceptions(message=self.error['message'], info=self.error['info'])
