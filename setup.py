from setuptools import setup
setup(name='datafunctions',
      version='0.1',
      description='Data Functions for Phrazor and Explorazor',
      url='https://gitlab.com/sabyasachi.nandy/datafunctions.git',
      author='Sabyasachi Nandy',
      author_email='sabyasachi.nandy@vphrase.com',
      license='MIT',
      packages=['data_functions'],
      zip_safe=False
)
